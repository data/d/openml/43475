# OpenML dataset: Trulia-Property-Listing-Dataset-2020

https://www.openml.org/d/43475

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This dataset was created by our in house Web Scraping and Data Mining teams at PromptCloud and DataStock. You can download the full dataset here. This sample contains 30K records.
Content
This dataset contains the following: 
Total Records Count: 78519 Domain Name: trulia.com Date Range :: 01st Jan 2020 - 31st Jan 2020  File Extension : csv
Available Fields:: Uniq Id, Crawl Timestamp, Url, Title, Description, Price, Image Url, Style, Sqr Ft, Longitude, Latitude, Home Id, Lot Size, Beds, Bath, Year Built, Price Sqr Ft, Features, Last Sold Year, Last Sold For, Last Tax Assestment, Last Tax Year, Address, City, State, Zipcode, Property Type, Address Full, Facts, Days On Trulia, Listing Agent Name, Listing Agent Contact No, Agent Name 1, Agent Contact No 1, Agent Name 2, Agent Contact No 2, Agent Name 3, Agent Contact No 3, Agent Name 4, Agent Contact No 4, Agent Name 5, Agent Contact No 5, Brokername, Image 1, Image 2, Image 3, Image 4, Image 5, Image 6, Image 7, Image 8, Image 9, Image 10, Image 11, Image 12, Image 13, Image 14, Image 15, Image 16, Image 17, Image 18, Image 19, Image 20, Image 21, Image 22, Image 23, Image 24, Image 25
Acknowledgements
We wouldn't be here without the help of our in house web scraping and data mining teams at PromptCloud and DataStock.
Inspiration
This dataset was created keeping in mind our data scientists and researchers across the world.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43475) of an [OpenML dataset](https://www.openml.org/d/43475). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43475/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43475/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43475/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

